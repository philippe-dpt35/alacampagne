Adapation du jeu "A la campagne" de la compilation d'applications Clicmenu.

L'activité consiste à constituer des collections d'objets selon une quantité donnée, de 1 à 10 ou de 10 à 20.

Elle peut être testée en ligne [ici](https://primtux.fr/applications/alacampagne/index.html)

Cette application a été co-développée par [mothsart](https://framagit.org/mothsart) qui est également l'auteur du graphisme : [accéder au dépôt des sources](https://framagit.org/mothsart/primtux_graphics)
